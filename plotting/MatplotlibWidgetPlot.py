# -*- coding: utf-8 -*-
# ===============================================================================
#
#
# Copyright (c) 2015 IST-SUPSI (www.supsi.ch/ist)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#
# ===============================================================================
import matplotlib
from PyQt4.QtGui import QWidget, QVBoxLayout
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg

if matplotlib.__version__ >= '1.5.0':
    from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
else:
    from matplotlib.backends.backend_qt4agg import NavigationToolbar2QTAgg as NavigationToolbar


class MatplotWidgetPlot(QWidget):
    """
        Class to plot data inside a QWidget
    """
    def __init__(self, parent=None):
        super(MatplotWidgetPlot, self).__init__(parent)

    def set_data(self, figure):
        """
            draw data to canvas

        Args:
            figure (figure) : matplotlib chart
        """

        canvas = FigureCanvasQTAgg(figure)

        vbox = QVBoxLayout(self)

        toolbar = NavigationToolbar(canvas, self)
        vbox.addWidget(toolbar)

        vbox.addWidget(canvas)
