# coding=utf-8
# Copyright (C) 2015 IDAEA-CSIC
#
# This program is free software; you can redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation; either version 2 of the License, 
# or (at your option) any later version. This program is distributed in the hope that it will be useful, but 
# ITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
# PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program; if not, write to 
# the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
'''
:copyright: Copyright (C) 2015 IDAEA-CSIC
:authors: V. Velasco Mansilla, L.M. de Vries, A. Nardi, R. Criollo, E. Vázquez Suñé
:contact: enric.vazquez@idaea.csic.es
:license: GPL General Public License, version 2 or any later version (http://opensource.org/licenses/GPL-2.0)
'''
from AKFeatureMeasurements import AKFeatureMeasurements
from model.SavedQueryMeasurementsModel import MEASUREMENT_RESULTS
from AKSettings import TABLES

class AKFeatureMeasurementsHydro(AKFeatureMeasurements):
    '''Make sure that all the queries used by AKFeatureMeasurements reference to the hydrogeological tables'''
    def __init__(self, settings, iface, windowTitle=u'Query Measurements', parent=None):
        super(AKFeatureMeasurementsHydro, self).__init__(settings, iface, windowTitle, parent)
        self.queryTable = TABLES.SAVED_QUERY_HYDRO
        
    def defineSQLQueries(self):
        self.sqlSelAllParams_1 = "SELECT distinct CP.nameEN, CP.parameterCode as name " \
                                "FROM " + TABLES.HYDROPARAMS + " CP "
        self.sqlSelAllParams_2 = "WHERE CP.parameterCode IN "
        self.sqlSelAllParams_3 = "ORDER BY CP.nameEN COLLATE NOCASE;"

        self.sqlSelMeasurements_1_1 = "SELECT " \
                                      "P.id AS PointId, " \
                                      "P.point AS Point, " \
                                      "CS.hydroPointObservation AS Observation, " \
                                      "CS.beginDate AS `Sample Date` , " \
                                      "'not apply' AS Campaign, " \
                                      "CM.resultTime `Measurement Date`, " \
                                      "CS.hydrogeologicalParametersCode paramId, " \
                                      "CP.parameterCode AS baseParamId, " \
                                      "CP.nameEN AS Parameter, " \
                                      "CM.value AS value, "  \
                                      "CM.id AS id, "  \
                                      "UNITS.nameEN AS Unit, " \
                                      "UNITS.uomCode AS UnitCode, " \
                                      "UNITS.id AS UnitId " \
                                    "FROM " + \
                                      TABLES.POINTS + " P, " + \
                                      TABLES.HYDROSAMPLES + " CS, " + \
                                      TABLES.HYDROMEASUREMENTS + " CM, " + \
                                      TABLES.L_UNITOFMEASUREMENTS + " UNITS, " + \
                                      TABLES.HYDROPARAMS + " CP, " + \
                                      TABLES.SAVED_QUERY_HYDRO + " SQ, " + \
                                      TABLES.SAVED_QUERY_POINTS_HYDRO + " SQP, " + \
                                      TABLES.SAVED_QUERY_SAMPLES_HYDRO + " SQS " + \
                                    "WHERE SQP.SavedQueryId = ? " \
                                      "AND SQ.ID = SQP.SAVEDQUERYID " \
                                      "AND SQP.ID = SQS.SavedQueryPointID " \
                                      "AND SQP.pointId = P.id " \
                                      "AND P.id = CS.pointId " \
                                      "AND SQS.Active = 1 " \
                                      "AND CS.id = SQS.SampleID " \
                                      "AND CS.beginDate BETWEEN SQ.StartDate AND SQ.EndDate " \
                                      "AND CM.hydroPointObsId = SQS.SampleID " \
                                      "AND CS.hydrogeologicalParametersCode = CP.id " \
                                      "AND CP.uomCode = UNITS.id " \
                                      "AND CP.parameterCode IN "
        
        self.sqlSelMeasurements_1_2 = ""
        self.sqlSelMeasurements_2 = " ORDER BY P.id, CS.beginDate, CP.nameEN;"

    def hideColumnsIfNecessary(self):
        self.m_form.tvResults.setColumnHidden(MEASUREMENT_RESULTS.Campaign, True)
        self.m_form.tvResults.setColumnHidden(MEASUREMENT_RESULTS.SampleDate, True)
