@ECHO OFF
IF EXIST %1 DEL %1
ECHO Creating AkvaGIS database with the following name: %1
spatialite.exe %1 < AkvaGISSchema.sql

