# -*- coding: utf-8 -*-
from PyQt4.QtCore import *
from PyQt4.QtGui import *

from qgis.core import *
from PyQt4.QtCore import QSettings, QTranslator, qVersion, QCoreApplication
from PyQt4.QtGui import QAction, QIcon, QMenu


# Initialize Qt resources from file resources.py
import sys, os
# add path
sys.path.append('C:\Users\iacopo.borsi\.qgis2\python\plugins\Freewat')


# Import the code for the dialog
from flopyaddon import ModflowZbd
from flopy.modflow import *
import numpy as np


# Load Modflow Model
namefile = 'C:\Users\iacopo.borsi\ex_t0\\fmptest.nam'
ml = Modflow.load(namefile, version='mf2005', model_ws='C:\Users\iacopo.borsi\ex_t0')

#namefile = 'C:\Users\iacopo.borsi\SIDGRID\Zonbud.3_01\Data\Zbtest.nam'
#ml = Modflow.load(namefile, version='mf2005', model_ws='C:\Users\iacopo.borsi\SIDGRID\Zonbud.3_01\Data')

nrow, ncol, nlay, nper = ml.nrow_ncol_nlay_nper

# Create the Zone dictionary

zn1 = np.ones(shape = (nrow, ncol))
zn1[2,3] = 2
zn_dict = {}
zn_dict[0] = zn1
#zn_dict[1] = zn2
#zn_dict[2] = zn1*3


# Define package and write file
print 'nome modello', ml.name
print [os.path.basename(ml.name).replace('', '')][0]
zb = ModflowZbd(ml, nzn = 2, zone_dict = zn_dict, exe_name = 'C:\Users\iacopo.borsi\SIDGRID\Zonbud.3_01\Bin\zonbud.exe')

zb.write_file()
zb.run_zonebudget()




