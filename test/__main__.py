# coding=utf-8

"""
run all tests

USAGE
    python -m freewat.test [-he]

OPTIONS
    -h
        print this help
    -e <test_1>,<test_2>
        exclude <test_1> and <test_2>


"""

import os
import re
from subprocess import Popen, PIPE
from time import time
import getopt
import sys

from . import test

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "he:",
            ["help"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

excludes = optlist['-e'].split(',') if '-e' in optlist else ['freewat.plotting.tests.plotting_test', 'freewat.plotting.tests.bubble_test']

if len(excludes):
    print "excluded", ' '.join(excludes)

start = time()

test(excludes)

print "\neverything is fine %d sec"%(int(time() - start))
