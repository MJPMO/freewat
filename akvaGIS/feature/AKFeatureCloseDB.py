#coding=utf-8
#Copyright (C) 2015 IDAEA-CSIC
#
#This program is free software; you can redistribute it and/or modify it under the terms of the 
#GNU General Public License as published by the Free Software Foundation; either version 2 of the License, 
#or (at your option) any later version. This program is distributed in the hope that it will be useful, but 
#ITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
#PURPOSE. See the GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License along with this program; if not, write to 
#the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
'''
:copyright: Copyright (C) 2015 IDAEA-CSIC
:authors: V. Velasco Mansilla, L.M. de Vries, A. Nardi, R. Criollo, E. Vázquez Suñé
:contact: enric.vazquez@idaea.csic.es
:license: GPL General Public License, version 2 or any later version (http://opensource.org/licenses/GPL-2.0)
'''
from qgis._core import QgsDataSourceURI, QgsVectorLayer, QgsMapLayerRegistry, QgsProject, QgsLayerTreeGroup
from PyQt4.QtGui import QIcon
from PyQt4 import QtGui
    
from AKFeature import AKFeature

class AKFeatureCloseDB (AKFeature):
    
    def __init__(self, settings, iface, dialogType, toolbarActions, parent=None):
        super(AKFeatureCloseDB, self).__init__(iface, parent)
        
        self.m_settings = settings
        self.dialogType = dialogType
        self.m_actions = toolbarActions
        self.m_icon = QIcon(settings.getIconPath('document-close.png'))
        self.m_text = "Close AkvaGIS Database"
        self.m_group = "AkvaGIS"

    def initialize(self):
        ret = self.confirmClosing()
        if ret == QtGui.QMessageBox.Yes:
            self.closeDatabase()
            self.updateButtons()

    def updateButtons(self):
        if(self.m_settings.currentDB == None):
            self.enableAkvaGIS(False)
        else:
            self.enableAkvaGIS(True)

    def enableAkvaGIS(self, setEnabled):
        notDisable = ["Create AkvaGIS Database", "Open AkvaGIS Database"]
        for action in self.m_actions:
            if(action.text() not in notDisable):
                action.setEnabled(setEnabled)    
