
18-02-2017
----------------------
- Fixed 256 row limitation
- Show icons bug in freewat fixed.
- Block edit id primary key in tables to avoid user input errors.