:: Installed OS
Set _Bit=64
IF %PROCESSOR_ARCHITECTURE% == x86 (
  IF NOT DEFINED PROCESSOR_ARCHITEW6432 Set _Bit=32

Echo installing pip....

python get_pip\get-pip.py


IF %_Bit% == 64 (
	Echo installing numpy for 64 bit...
	pip install numpy\numpy-1.11.0+mkl-cp27-cp27m-win_amd64.whl
) ELSE (
	Echo installing numpy for 32 bit....
	pip install numpy\numpy-1.11.0+mkl-cp27-cp27m-win32.whl
)

Echo installing pandas.....
pip install pandas==0.18.0

Echo Installing isodate....
pip install isodate

Echo installing requests....
pip install requests

Echo installing matplotlib...

pip install matplotlib

Echo installing flopy....
pip install flopy==3.2.4 

Echo installing xlrd....
pip install xlrd

Echo installing xlwt....
pip install xlwt

Echo installing Seaborn....
pip install seaborn


pause

