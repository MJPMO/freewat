#-------------------------------------------------------------------------------
# Name:        modulo1
# Purpose:
#
# Author:      iacopo.borsi
#
# Created:     18/02/2015
# Copyright:   (c) iacopo.borsi 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------
# Initialize Qt resources from file resources.py
import sys, os
# add path
sys.path.append('C:\Users\iacopo.borsi\.qgis2\python\plugins\freewat')
# sys.path.append('/home/matteo/.qgis2/python/plugins/freewat')
from sqlite_utils import uploadCSV

dbname = 'C:\Users\iacopo.borsi\ex_t0\t0.sqlite'
tableName = 'paperino'
filename = 'C:\Users\iacopo.borsi\ex_t0\input_data\river_parameters\riv_par_2sp.csv'
uploadCSV(dbName, filename, tableName )
