Plugin directory structure
==========================

* freewat_utils: methods used in several modules
* sqlite_utils: methods used in several modules for connection to/from DB Spatialite
* mdoCreate_utils: it contains all methods to create Model, DB, MDOs and other useful stuff


DEPENDENCIES
============
Flopy Version 3 has to be installed.
In turn, dependencies needed by Flopy are listed at Flopy `web site <https://github.com/modflowpy/flopy/blob/master/readme.md>`_

Install **FREEWAT** and all the realted dependencies
----------------------------------------------------

1. Unzip the ``freewat-version_0_2.zip`` file

2. Copy the extracted ``freewat`` folder in the ``C:\Users\your_user_name\.qgis2\python\plugins`` folder

.. note:: if you don't have the ``plugins`` directory, you can manually create it

3. Run QGIS. Some pop-up windows will appear and will inform you that some required packages are going to be installed. Just type **OK** to each window