#coding=utf-8
#Copyright (C) 2015 IDAEA-CSIC
#
#This program is free software; you can redistribute it and/or modify it under the terms of the 
#GNU General Public License as published by the Free Software Foundation; either version 2 of the License, 
#or (at your option) any later version. This program is distributed in the hope that it will be useful, but 
#ITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
#PURPOSE. See the GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License along with this program; if not, write to 
#the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
'''
:copyright: Copyright (C) 2015 IDAEA-CSIC
:authors: V. Velasco Mansilla, L.M. de Vries, A. Nardi, R. Criollo, E. Vázquez Suñé
:contact: enric.vazquez@idaea.csic.es
:license: GPL General Public License, version 2 or any later version (http://opensource.org/licenses/GPL-2.0)
'''
from PyQt4.QtGui import QColor, QDataWidgetMapper

from AKFeatureResults import AKFeatureResults
from form.AKWidgetGeneralPlotSettings import AKWidgetGeneralPlotSettings
from model.PlotSettingsModel import PlotSettingsModel
from external.chemPlotLib.ChemPlot import CPSETTINGS

class AKFeatureResultsPlot(AKFeatureResults):
    '''
    This feature provides the base functionality for the second form to create plots. 
    '''
    def __init__(self, settings, iface, plotClass, windowTitle = "", parent=None):
        self.plotClass = plotClass
        super(AKFeatureResultsPlot, self).__init__(settings, iface, windowTitle, parent)

    def customizeForm(self):
        super(AKFeatureResultsPlot, self).customizeForm()
        
        self.plotSettingsForm = AKWidgetGeneralPlotSettings(self.iface, self.m_form)  
        self.m_form.customResultsGroup.show()
        self.m_form.customResultsGroup.setTitle("Plot configuration")        
        self.m_form.customResultsGroup.layout().addWidget(self.plotSettingsForm)
        
        myPlot = self.plotClass()
        settings = myPlot.settings() 
        
        self.plotSettingsModel = PlotSettingsModel()
        mod = self.plotSettingsModel 
        mod.setInitialData(settings) 
         
        self.mapper = QDataWidgetMapper(self)                 
        self.mapper.setModel(mod)
        self.mapper.setSubmitPolicy(QDataWidgetMapper.AutoSubmit)
        
        window_X_size_id =  mod.columnFromField(CPSETTINGS.window, CPSETTINGS.window_X_size_id)
        window_Y_size_id =  mod.columnFromField(CPSETTINGS.window, CPSETTINGS.window_Y_size_id)         
        window_DPI_id =  mod.columnFromField(CPSETTINGS.window, CPSETTINGS.window_DPI_id)        
         
        self.mapper.addMapping(self.plotSettingsForm.window_X_size, window_X_size_id)
        self.mapper.addMapping(self.plotSettingsForm.window_Y_size, window_Y_size_id)
        self.mapper.addMapping(self.plotSettingsForm.window_DPI, window_DPI_id)                
 
        title_show_id =  mod.columnFromField(CPSETTINGS.title, CPSETTINGS.title_show_id)
        title_label_id =  mod.columnFromField(CPSETTINGS.title, CPSETTINGS.title_label_id)         
        title_font_size_id =  mod.columnFromField(CPSETTINGS.title, CPSETTINGS.title_font_size_id)
        title_font_color_id =  mod.columnFromField(CPSETTINGS.title, CPSETTINGS.title_font_color_id)
        title_font_type_id =  mod.columnFromField(CPSETTINGS.title, CPSETTINGS.title_font_type_id)         
         
        self.mapper.addMapping(self.plotSettingsForm.titleGroup, title_show_id, "checked")
        self.mapper.addMapping(self.plotSettingsForm.title_label, title_label_id)
        self.mapper.addMapping(self.plotSettingsForm.title_font_size, title_font_size_id)
        self.mapper.addMapping(self.plotSettingsForm.title_font_color, title_font_color_id)
        self.mapper.addMapping(self.plotSettingsForm.title_font_type, title_font_type_id)
         
        marker_show_id =  mod.columnFromField(CPSETTINGS.marker_plot, CPSETTINGS.marker_show_id)
        marker_type_serie_id =  mod.columnFromField(CPSETTINGS.marker_plot, CPSETTINGS.marker_type_serie_id)         
        marker_color_serie_id =  mod.columnFromField(CPSETTINGS.marker_plot, CPSETTINGS.marker_color_serie_id)
        marker_size_id =  mod.columnFromField(CPSETTINGS.marker_plot, CPSETTINGS.marker_size_id)
        marker_edge_width_id =  mod.columnFromField(CPSETTINGS.marker_plot, CPSETTINGS.marker_edge_width_id)   
         
        self.mapper.addMapping(self.plotSettingsForm.markerGroup, marker_show_id, "checked")
        self.mapper.addMapping(self.plotSettingsForm.marker_type_serie, marker_type_serie_id)
        self.mapper.addMapping(self.plotSettingsForm.marker_color_serie, marker_color_serie_id)
        self.mapper.addMapping(self.plotSettingsForm.marker_size, marker_size_id)
        self.mapper.addMapping(self.plotSettingsForm.marker_edge_width, marker_edge_width_id)   
         
        line_show_id =  mod.columnFromField(CPSETTINGS.line_plot, CPSETTINGS.line_show_id)
        line_type_serie_id =  mod.columnFromField(CPSETTINGS.line_plot, CPSETTINGS.line_type_serie_id)         
        line_color_serie_id =  mod.columnFromField(CPSETTINGS.line_plot, CPSETTINGS.line_color_serie_id)
        line_width_id =  mod.columnFromField(CPSETTINGS.line_plot, CPSETTINGS.line_width_id)        
         
        self.mapper.addMapping(self.plotSettingsForm.lineGroup, line_show_id, "checked")
        self.mapper.addMapping(self.plotSettingsForm.line_type_serie, line_type_serie_id)
        self.mapper.addMapping(self.plotSettingsForm.line_color_serie, line_color_serie_id)
        self.mapper.addMapping(self.plotSettingsForm.line_width, line_width_id)
        
        legend_show_id =  mod.columnFromField(CPSETTINGS.legend, CPSETTINGS.legend_show_id)
        legend_num_columns_automatic_id =  mod.columnFromField(CPSETTINGS.legend, CPSETTINGS.legend_num_columns_automatic_id)
        legend_num_columns_id =  mod.columnFromField(CPSETTINGS.legend, CPSETTINGS.legend_num_columns_id)
        legend_markerscale_id =  mod.columnFromField(CPSETTINGS.legend, CPSETTINGS.legend_markerscale_id)
        legend_fontsize_id =  mod.columnFromField(CPSETTINGS.legend, CPSETTINGS.legend_fontsize_id)
        #legend_shadow_id =  mod.columnFromField(CPSETTINGS.legend, CPSETTINGS.legend_shadow_id)                 
        
        self.mapper.addMapping(self.plotSettingsForm.legendGroup, legend_show_id, "checked")
        self.mapper.addMapping(self.plotSettingsForm.legend_num_columns_automatic, legend_num_columns_automatic_id)
        self.mapper.addMapping(self.plotSettingsForm.legend_num_columns, legend_num_columns_id)
        self.mapper.addMapping(self.plotSettingsForm.legend_markerscale, legend_markerscale_id)
        self.mapper.addMapping(self.plotSettingsForm.legend_fontsize, legend_fontsize_id)
        #self.mapper.addMapping(self.plotSettingsForm.legend_shadow, legend_shadow_id)         

        self.mapper.toFirst()
        self.plotSettingsForm.title_font_color_picker.setConnections(self.plotSettingsModel, self.plotSettingsForm.title_font_color, title_font_color_id)
        self.plotSettingsForm.title_font_color_picker.setColor(QColor(self.plotSettingsForm.title_font_color.text()))
        self.plotSettingsForm.title_font_color.hide()
                 
        currentText = mod.data(mod.index(0,title_font_type_id))
        comboW = self.plotSettingsForm.title_font_type
        comboW.setCurrentIndex(comboW.findText(currentText))
        
        currentText = mod.data(mod.index(0,marker_type_serie_id))
        comboW = self.plotSettingsForm.marker_type_serie
        comboW.setCurrentIndex(comboW.findText(currentText))
        
        currentText = mod.data(mod.index(0,line_type_serie_id))
        comboW = self.plotSettingsForm.line_type_serie
        comboW.setCurrentIndex(comboW.findText(currentText))        
        
        currentText = mod.data(mod.index(0,marker_color_serie_id))
        comboW = self.plotSettingsForm.marker_color_serie
        comboW.setCurrentIndex(comboW.findText(currentText))        
        
        currentText = mod.data(mod.index(0,line_color_serie_id))
        comboW = self.plotSettingsForm.line_color_serie
        comboW.setCurrentIndex(comboW.findText(currentText))

