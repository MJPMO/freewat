import unittest
from plotting import DataReader as Dr
from plotting import PlottingAdapter as ad
import seaborn as sns
import os

class TestCSVMethods(unittest.TestCase):
    path = 'C:\\Users\\LauraF\\.qgis2\python\\plugins\\freewat\\plotting\\tests\\'
    file1 = path + "S4old_5._sc"
    file2 = path + "S4old_5._sd"
    file3 = path + "t_syn_out._ww"
    file4 = path + "t_syn_out._ws"
    file5 = path + "t_syn_out._ss"
    file6 = path + "t_syn_out._sc_svd"
    file7 = path + "t_syn_out._pcc"
    file8 = path + "t_syn_out._so"
    file9 = path + "t_syn_out._w"
    file10 = path + "t_syn_out._r"
    file11 = path + "t_syn_out._s1"
    file12 = path + "t_syn_out._svd"
    file13 = path + "t_syn_out._os"
    file14 = path + "t_syn_out._scgrp"
    file15 = path + "t_syn_out._pa"

    def test_read_calibration(self):
        data1 = Dr.Reader.read_calibration(self.file1)
        data2 = Dr.Reader.read_calibration(self.file2)
        self.assertEqual(data1['COMPOSITE SCALED SENSITIVITY'][0], 5.3156953875163797)
        self.assertEqual(data2['KRIVER1'][0], 13.454999960752831)

    def test_bar(self):
        data = Dr.Reader.read_calibration(self.file1)
        x, y = Dr.Reader().modify_data_sc(data)
        plot = ad.PlottingAdapter.plot_bar(x, y)
        sns.plt.ylabel('some numbers')
        plot.savefig(self.path +'plot.pdf')
        plot.clf()

    def test_facet(self):
        data = Dr.Reader.read_calibration(self.file2)
        pf = Dr.Reader().modify_data_sd(data, [])
        plot = ad.PlottingAdapter.plot_facet(pf)
        plot.savefig(self.path +'plot2.pdf')
        sns.plt.close()

    def test_multiple(self):
        data = Dr.Reader.read_calibration(self.file2)
        pf = Dr.Reader().modify_data_sd(data, [])
        plot = ad.PlottingAdapter.plot_multiple_in_one(pf)
        plot.savefig(self.path +'plot3.pdf')
        sns.plt.close()

    def test_specific(self):
        data = Dr.Reader.read_calibration(self.file2)
        pf = Dr.Reader().modify_data_sd(data, ['KRIVER1', 'KRIVER2'])
        plot = ad.PlottingAdapter.plot_multiple_in_one(pf)
        plot.savefig(self.path +'plot4.pdf')
        sns.plt.close()

    def test_scatter(self):
        data = Dr.Reader.read_calibration(self.file3)
        plot = ad.PlottingAdapter.plot_scatter_ww(data)
        plot.savefig(self.path +'plot5.pdf')
        sns.plt.close()

    def test_scatter2(self):
        data = Dr.Reader.read_calibration(self.file4)
        plot = ad.PlottingAdapter.plot_scatter_ws(data)
        plot.savefig(self.path +'plot6.pdf')
        sns.plt.close()

    def test_line1(self):
        data = Dr.Reader.read_calibration(self.file5)
        plot = ad.PlottingAdapter.plot_multiple_lines_annotated(data)
        plot.savefig(self.path +'plot7.pdf')
        sns.plt.close()

    def test_stacked_bar1(self):
        data = Dr.Reader.read_calibration_without_first_and_last(self.file6)
        plot = ad.PlottingAdapter.plot_bar_stacked(data)
        plot.savefig(self.path +'plot8.pdf')
        sns.plt.close()

    def test_pos_neg(self):
        data = Dr.Reader.read_calibration(self.file7)
        plot = ad.PlottingAdapter.plot_bar_positive_negative(data)
        plot.savefig(self.path +'plot9.pdf')
        sns.plt.close()

    def test_bar_PCC(self):
        data = Dr.Reader.read_calibration(self.file8)
        x, y = Dr.Reader().modify_data_so(data)
        plot = ad.PlottingAdapter.plot_bar(x, y)
        plot.savefig(self.path +'plot10.pdf')
        sns.plt.close()

    def test_bar_w(self):
        data = Dr.Reader.read_calibration(self.file9)
        x, y = Dr.Reader().modify_data_w(data)
        plot = ad.PlottingAdapter.plot_bar(x, y)
        plot.savefig(self.path +'plot11.pdf')
        sns.plt.close()

    def test_bar_r(self):
        data = Dr.Reader.read_calibration(self.file10)
        x, y = Dr.Reader().modify_data_r(data)
        plot = ad.PlottingAdapter.plot_bar(x, y)
        plot.savefig(self.path +'plot12.pdf')
        sns.plt.close()

    def test_line(self):
        data = Dr.Reader.read_calibration(self.file11)
        plot = ad.PlottingAdapter.plot_lines(data)
        plot.savefig(self.path +'plot13.pdf')
        sns.plt.close()

    def test_svd(self):
        data = Dr.Reader.read_calibration_without_first4(self.file12)
        plot = ad.PlottingAdapter.plot_bar_stacked2(data)
        plot.savefig(self.path +'plot14.pdf')
        sns.plt.close()

    def test_os(self):
        data = Dr.Reader.read_calibration(self.file13)
        plot = ad.PlottingAdapter.plot_scatter_os(data)
        plot.savefig(self.path +'plot15.pdf')
        sns.plt.close()

    def test_scgrp(self):
        data, groups = Dr.Reader.read_scgrp(self.file14)
        plot = ad.PlottingAdapter.plot_bar_stacked_scgrp(data, groups)
        plot.savefig(self.path +'plot16.pdf')
        sns.plt.close()

    def test_pa(self):
        data = Dr.Reader.read_pa(self.file15)
        plot = ad.PlottingAdapter.plot_lines_itter(data, False)
        plot.savefig(self.path +'plot17.pdf')
        sns.plt.close()

    def test_pa2(self):
        data = Dr.Reader.read_pa(self.file15)
        plot = ad.PlottingAdapter.plot_lines_itter(data, True)
        plot.savefig(self.path +'plot18.pdf')
        sns.plt.close()

if __name__ == '__main__':
    unittest.main()
